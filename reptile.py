#!/usr/bin/env python
# -*-coding:utf-8 -*-

import os
import time
import logging
import contextlib
import cx_Oracle

os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'


class OracleConnect(object):

    def __init__(self, user, password, database):
        self.user = user
        self.pwd = password
        self.database = database

    def conn_oracle(self):
        try:
            conn = cx_Oracle.connect(self.user, self.pwd, self.database)
        except cx_Oracle.DatabaseError as e:
            print("Database Connection Exception, Try Again...", e)
            time.sleep(2)
            conn = cx_Oracle.connect(self.user, self.pwd, self.database)
        return conn

    @contextlib.contextmanager
    def execute_oracle(self):
        conn = self.conn_oracle()
        cursor = conn.cursor()
        try:
            yield cursor
            conn.commit()
        except cx_Oracle.DatabaseError as e:
            conn.rollback()
            logging.error('oracle_py error is %s' % e)
            print('error:', e)


class GenData(object):

    def data_initial(self, user, pwd, database, sql):
        oc = OracleConnect(user, pwd, database)
        with oc.execute_oracle() as cursor:
            cursor.execute(sql)
        row = cursor.fetchone()
        while row:
            yield row
            row = cursor.fetchone()[dg_sch@localhost zgw]$ 